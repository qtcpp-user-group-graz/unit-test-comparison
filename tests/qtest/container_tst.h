#include <QObject>
#include <QtTest>

template<typename T>class Container;

class TestContainer: public QObject
{
    Q_OBJECT
private slots:
    void init();
    void cleanup();

    void initialStatus();
    void addItem_data();
    void addItem();
    void getItem();
    void removeItem();

    void addRemoveBenchmark();

private:
    Container<int>* m_container = 0;
};
