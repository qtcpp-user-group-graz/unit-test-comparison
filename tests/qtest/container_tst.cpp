#include "container_tst.h"

#include <container.h>

void TestContainer::init()
{
    m_container = new Container<int>;
}

void TestContainer::cleanup()
{
    delete m_container;
    m_container = nullptr;
}

void TestContainer::initialStatus()
{
    QCOMPARE(m_container->size(), 0);
}

void TestContainer::addItem_data()
{
    QTest::addColumn<std::vector<int>>("items");
    QTest::addColumn<int>("size");

    QTest::newRow("one item") << std::vector<int>{1} << 1;
    QTest::newRow("3 tiems") << std::vector<int>{1,2,3} << 3;
    QTest::newRow("5 items") << std::vector<int>{1,2,3,4,5} << 5;
}

void TestContainer::addItem()
{
    QFETCH(std::vector<int>, items);
    QFETCH(int, size);

    for (const auto& item : items) {
        m_container->add(item);
    }
    QCOMPARE(m_container->size(), size);
}

void TestContainer::getItem()
{
    m_container->add(3);
    m_container->add(4);
    m_container->add(5);
    QCOMPARE(m_container->get(1), 4);
}

void TestContainer::removeItem()
{
    m_container->add(3);
    m_container->add(4);
    m_container->add(5);
    QCOMPARE(m_container->remove(1), true);
    QCOMPARE(m_container->size(), 2);
    QCOMPARE(m_container->get(1), 5);
}

void TestContainer::addRemoveBenchmark()
{
    QBENCHMARK {
        m_container->add(667);
        m_container->remove(0);
    }
}

QTEST_MAIN(TestContainer)
