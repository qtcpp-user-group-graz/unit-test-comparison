add_executable (googleTest
    container_google_tst.cpp
)

add_test(NAME googleTest COMMAND googleTest)

target_link_libraries(googleTest
    gtest
    gtest_main
    simple
)
