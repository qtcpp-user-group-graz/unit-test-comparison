add_library(simple STATIC
    container.h
    container.cpp
)

target_include_directories(simple
    PUBLIC .)
